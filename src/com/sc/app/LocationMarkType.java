package com.sc.app;

public class LocationMarkType {
	public static final int MARK_AS_BEGIN = 0;
	public static final int MARK_AS_MIDDLE = 1;
	public static final int MARK_AS_END = 2;
	public static final int MARK_CANCEL = 3;
	public static final int MARK_UNDEFINE = 0xFF;
}
