package com.sc.map;

import java.io.File;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import com.baidu.mapapi.map.BaiduMap;
import com.sc.lane.R;
import com.sc.utils.FileUtil;

public class SnapshotDoneListener implements BaiduMap.SnapshotReadyCallback{
	private ProgressDialog myProgressDialog = null;
	private Context mContext = null;
	
	public SnapshotDoneListener(Context ctx){
		mContext = ctx;
		myProgressDialog = ProgressDialog.show(mContext, 
				mContext.getResources().getString(R.string.snapshot_progress_title),
				mContext.getResources().getString(R.string.snapshot_progress_content));
	}
	
	@Override
	public void onSnapshotReady(Bitmap arg0) {
		if(arg0 != null){
			String fileName = String.valueOf(System.currentTimeMillis()) + ".jpg";
			String path = FileUtil.savePicToSdcard(arg0, fileName);
			
			myProgressDialog.dismiss();
			
			this.shareMsg(path);
		}
	}
	
	public void shareMsg(String imgPath) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		File f = new File(imgPath);
		if (f != null && f.exists() && f.isFile()) {
			Uri uri = Uri.fromFile(f);
			intent.putExtra(Intent.EXTRA_STREAM, uri);  
			intent.setType("image/*"); 
		}
		mContext.startActivity(Intent.createChooser(intent, 
				mContext.getResources().getString(R.string.share)));
	}
}
