package com.sc.adapter;

import java.util.ArrayList;
import java.util.List;

import com.sc.app.Config;
import com.sc.db.DBStorage;
import com.sc.db.MarkPoint;
import com.sc.lane.R;
import com.sc.utils.BitmapUtil;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridPhotoAdapter extends BaseAdapter{
	private Activity mActivity;
	private List<MarkPoint> mPoints;
	private List<String> mPhotos;
	private LayoutInflater mInflater;

	public GridPhotoAdapter(Activity activity, int recordId) {
		this.mActivity = activity;
		this.mPoints = DBStorage.getMarkPoints(mActivity, recordId);
		this.mInflater = mActivity.getLayoutInflater();
		this.mPhotos = new ArrayList<String>();
		
		for(MarkPoint point : mPoints){
			String str = point.getPhotos();
			if(str != null){
				String[] ps = str.split(",");
				for(String s : ps){
					mPhotos.add(s);
				}
			}
		}
	}

	@Override
	public int getCount() {
		return mPhotos.size();
	}

	@Override
	public Object getItem(int position) {
		return mPhotos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = mInflater.inflate(R.layout.main_fragement_photos_grid_item, null);
		
		ImageView imageView = (ImageView)convertView.findViewById(R.id.iv_photo);
		TextView title = (TextView)convertView.findViewById(R.id.tv_title);
		
		String photo = mPhotos.get(position);
		Bitmap bitmap = BitmapFactory.decodeFile(Config.PHOTO_FOLDER + photo);
		if(bitmap != null){
			imageView.setImageBitmap(BitmapUtil.cutterBitmap(bitmap, 100, 100));
		}
		else{
			imageView.setImageBitmap(BitmapFactory.decodeResource(mActivity.getResources(), 
					R.drawable.ic_launcher));
		}
		
		title.setText("û�м�¼����Ƭ");
		
		return convertView;
	}

}
